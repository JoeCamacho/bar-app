import React from "react";
import MenuItem from "./MenuItem";

class Menu extends React.Component {
  render() {
    return (
      <div>
        <button>Order</button>
        {this.props.drinks.map(drink => {
          return <MenuItem drink={drink} key={drink.id}/>;
        })}
      </div>
    );
  }
}

export default Menu;
