import React, { Component } from "react";

class Header extends Component {
  render() {
    return (
      <div>
        <img
          src={require("../assets/img/bar-header.jpg")}
          className="header-image"
          alt="Bar"
        />
      </div>
    );
  }
}

export default Header;
