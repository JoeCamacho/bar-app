import React, { Component } from "react";

class MenuItem extends Component {
  render() {
    return (
      <div>
        <img src={this.props.drink.image} alt={this.props.drink.name} />
        <div>
          <h2>{this.props.drink.name}</h2>
          <p>{this.props.drink.category}</p>
          <h4>Price:</h4>
          <p>{this.props.drink.price} euro</p>
        </div>
      </div>
    );
  }
}

export default MenuItem;
