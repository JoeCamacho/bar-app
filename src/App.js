import React from "react";
import Header from "./components/Header";
import Menu from "./components/Menu";
import { drinks } from "./data";

import "./App.css";

class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Menu drinks={drinks} />
      </div>
    );
  }
}

export default App;
